import pygame, sys, os
from pygame.locals import *
from abc import ABC, abstractmethod

clock = pygame.time.Clock()


########design pattern observer##############

class Observable(ABC):

    @abstractmethod
    def register(self, observer: ABC) -> None:
        pass

    @abstractmethod
    def unregister(self, observer: ABC) -> None:
        pass

    @abstractmethod
    def dispatch(self) -> None:
        pass


class Observer(ABC):

    @abstractmethod
    def update(self, observable: Observable) -> None:
        pass


class Player(Observable):
    def __init__(self):
        self.isDead = False
        self.Observers = set()

    def register(self, observer: ABC) -> None:
        self.Observers.add(observer)

    def unregister(self, observer: ABC) -> None:
        self.Observers.remove(observer)

    def dispatch(self) -> None:
        for observer in self.Observers:
            observer.update(self)

    def die(self):
        self.isDead = True
        self.dispatch()


class FinalWord(Observer):
    def __init__(self):
        self.message = "Victoire !Haut Fait atteint : IMMORTEL "

    def update(self, observable: Observable) -> None:
        self.message = "Victoire ! pas de haut fait car vous etes mort ... "

    def display(self, x, y, screen):
      #  self.text = font.render(self.message, True, (255, 255, 255), (0, 0, 0))
      #  self.textRect = self.text.get_rect()
      # self.textRect.center = (x, y - 50)
      #  screen.blit(self.text, self.textRect)
        print(self.message)


#########################################################################
#################### Design Template method #############################

class AbstractEnnemy(ABC):

    def template_method(self,scroll, rect,event) -> None:

        self.set_image()
        self.move(scroll,rect)


    def set_image(self):
        self.img = pygame.image.load("player_animations/ennemi.png")


    @abstractmethod
    def move(self,scroll, rect) -> None:
       pass


    def state(self,state):
        self.state = state




class BadGuy1(AbstractEnnemy):

   def move(self,scroll,rect) -> None:
       self.img = pygame.transform.scale(self.img, (45, 40))

       display.blit(pygame.transform.flip(self.img, False, False),
                    (rect.x - scroll[0] , rect.y - scroll[1] )) #580 / 879 - 179



class BadGuy2(AbstractEnnemy):

    def __init__(self):
        self.state = idle()

    def move(self,scroll,rect) -> None:
        self.img = pygame.transform.scale(self.img, (65, 60))

        display.blit(pygame.transform.flip(self.img, False, False),
                     (rect.x - scroll[0], rect.y - scroll[1]))  # 580 / 879 - 179

    def on_event(self, event):
         self.state = self.state.on_event(event)

#########################################################################
################## Design FSM ###########################################

class State(object):
    """
    We define a state object which provides some utility functions for the
    individual states within the state machine.
    """

    def __init__(self):
        print('Processing current state:', str(self))

    def on_event(self, event):

        pass

    def __repr__(self):

        return self.__str__()

    def __str__(self):

        return self.__class__.__name__

class Jumping(State):

    def on_event(self, event):
        if event == USEREVENT+2:
            return idle()

        return self


class idle(State):

    def on_event(self, event):
        if event == USEREVENT+1:
            return Jumping()

        return self




#########################################################################


pygame.init()  # initiates pygame

pygame.display.set_caption('Bario Moss Game')

WINDOW_SIZE = (600, 400)

screen = pygame.display.set_mode(WINDOW_SIZE, 0, 32)  # initiate the window
font = pygame.font.Font('freesansbold.ttf', 32)  # font for writing

display = pygame.Surface((300, 200))

moving_right = False
moving_left = False
vertical_momentum = 0
air_timer = 0


true_scroll = [0, 0]
winning_tiles = []

######### appel pour l'observable ###########
player1 = Player()
MessageFin = FinalWord()
player1.register(MessageFin)

############################################

bomb1 = BadGuy1()
bomb2 = BadGuy2()
b1_rect = pygame.Rect(496, 168, 45, 30)
b2_rect = pygame.Rect(870, 169, 65, 60)




def load_map(path):
    f = open(path + '.txt', 'r')
    data = f.read()
    f.close()
    data = data.split('\n')
    game_map = []
    for row in data:
        game_map.append(list(row))
    return game_map


global animation_frames
animation_frames = {}


def load_animation(path, frame_durations):
    global animation_frames
    animation_name = path.split('/')[-1]
    animation_frame_data = []
    n = 1
    for frame in frame_durations:
        animation_frame_id = animation_name + '__' + str(n)
        img_loc = path + '/' + animation_frame_id + '.png'

        animation_image = pygame.image.load(img_loc)

        animation_frames[animation_frame_id] = animation_image.copy()
        for i in range(frame):
            animation_frame_data.append(animation_frame_id)
        n += 1
    return animation_frame_data


def change_action(action_var, frame, new_value):
    if action_var != new_value:
        action_var = new_value
        frame = 0
    return action_var, frame


animation_database = {}

animation_database['run'] = load_animation('player_animations/walk',
                                           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                            1, 1, 1, 1, 1, 1])
animation_database['jump'] = load_animation('player_animations/jump',
                                            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                             1, 1, 1, 1, 1, 1])
animation_database['idle'] = load_animation('player_animations/idle', [20])

game_map = load_map('map')

grass_img = pygame.image.load('grass.png')
dirt_img = pygame.image.load('dirt.png')
roc_img = pygame.image.load("roc.PNG")

player_action = 'idle'
player_frame = 0
player_flip = False
player_mass = 1

player_rect = pygame.Rect(100, 100, 15, 30)

background_objects = [[0.25, [120, 10, 70, 400]], [0.25, [280, 30, 40, 400]], [0.5, [30, 40, 40, 400]],
                      [0.5, [130, 90, 100, 400]], [0.5, [300, 80, 120, 400]]]

def collision_test(rect, tiles, ):
    hit_list = []
    for tile in tiles:
        if rect.colliderect(tile):
            hit_list.append(tile)

        for tiled in winning_tiles:
            if rect.colliderect(tiled):
                MessageFin.display(player_rect.x, player_rect.y - 100,display)

    return hit_list



def move(rect, movement, tiles, ):
    collision_types = {'top': False, 'bottom': False, 'right': False, 'left': False}
    rect.x += movement[0]
    hit_list = collision_test(rect, tiles,)


    for tile in hit_list:
        if movement[0] > 0:
            rect.right = tile.left
            collision_types['right'] = True
        elif movement[0] < 0:
            rect.left = tile.right
            collision_types['left'] = True

    rect.y += movement[1]
    hit_list = collision_test(rect, tiles,)
    for tile in hit_list:
        if movement[1] > 0:
            rect.bottom = tile.top
            collision_types['bottom'] = True
        elif movement[1] < 0:
            rect.top = tile.bottom
            collision_types['top'] = True
    return rect, collision_types





# winning tile
y = 0
for layer in game_map:
    x = 0
    for tile in layer:
        if tile == '3':
            winning_tiles.append(pygame.Rect(x * 16, y * 16, 16, 16))
        x += 1
    y += 1

jump = 0
pygame.time.set_timer(USEREVENT+1, 4000)
pygame.time.set_timer(USEREVENT+2, 4900)
event_state = USEREVENT+1

while True:  # game loop
    display.fill((146, 244, 255))  # clear screen by filling it with blue

    true_scroll[0] += (player_rect.x - true_scroll[0] - 120) / 20
    true_scroll[1] += (player_rect.y - true_scroll[1] - 106) / 20
    scroll = true_scroll.copy()
    scroll[0] = int(scroll[0])
    scroll[1] = int(scroll[1])

    pygame.draw.rect(display, (7, 80, 75), pygame.Rect(0, 120, 300, 80))
    for background_object in background_objects:
        obj_rect = pygame.Rect(background_object[1][0] - scroll[0] * background_object[0],
                               background_object[1][1] - scroll[1] * background_object[0], background_object[1][2],
                               background_object[1][3])
        if background_object[0] == 0.5:
            pygame.draw.rect(display, (14, 222, 150), obj_rect)
        else:
            pygame.draw.rect(display, (9, 91, 85), obj_rect)

    tile_rects = []
    y = 0
    for layer in game_map:
        x = 0
        for tile in layer:
            if tile == '2':
                display.blit(dirt_img, (x * 16 - scroll[0], y * 16 - scroll[1]))
            if tile == '1':
                display.blit(grass_img, (x * 16 - scroll[0], y * 16 - scroll[1]))
            if tile == '3':
                display.blit(roc_img, (x * 16 - scroll[0], y * 16 - scroll[1]))
                # winning_tiles.append(pygame.Rect(x*16,y*16,16,16))

            if tile != '0':
                tile_rects.append(pygame.Rect(x * 16, y * 16, 16, 16))
            x += 1
        y += 1

    player_movement = [0, 0]
    ennemy_mouvement = [0, 0]
    if moving_right == True:
        player_movement[0] += 2
    if moving_left == True:
        player_movement[0] -= 2
    player_movement[1] += vertical_momentum


    vertical_momentum += player_mass*0.2  #gravity simulation
    jump += 0.2
    b2_rect.y += jump
    if vertical_momentum > 3:
        vertical_momentum = 3
    if jump > 2:
        jump = 2

    if player_movement[0] == 0:
        player_action, player_frame = change_action(player_action, player_frame, 'idle')
    if player_movement[0] > 0:
        player_flip = False
        player_action, player_frame = change_action(player_action, player_frame, 'run')
    if player_movement[0] < 0:
        player_flip = True
        player_action, player_frame = change_action(player_action, player_frame, 'run')
    if player_movement[1] < 0:
        player_action, player_frame = change_action(player_action, player_frame, 'jump')
    if player_rect.y > 550:  # reset when falling
        player_rect.x = 100;
        player_rect.y = 100;
        player1.die()


    player_rect, collisions = move(player_rect, player_movement, tile_rects)

    if collisions['bottom'] == True:
        air_timer = 0
        vertical_momentum = 0
    else:
        air_timer += 1

    if b2_rect.y > 169:
        jump = 0
        b2_rect.y = 169
        #if jump == 0:
        # bomb2.on_event("USEREVENT")





    player_frame += 1
    if player_frame >= len(animation_database[player_action]):
        player_frame = 0

    player_img_id = animation_database[player_action][player_frame]
    player_img = animation_frames[player_img_id]
    player = pygame.transform.scale(player_img, (15, 30))
    display.blit(pygame.transform.flip(player, player_flip, False),
                 (player_rect.x - scroll[0], player_rect.y - scroll[1]))

    bomb1.template_method(scroll,b1_rect,event_state)
    bomb2.template_method(scroll,b2_rect,event_state)
    bomb2.on_event(event_state)

    for event in pygame.event.get():
        event_state = event.type
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event_state == USEREVENT+1:
            jump = -5
            print("jump")
        if event_state==USEREVENT+2:
            bomb2.on_event(event_state)
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
            if event.key == K_RIGHT:
                moving_right = True
            if event.key == K_LEFT:
                moving_left = True
            if event.key == K_UP:
                if air_timer < 6:
                    vertical_momentum = -5
        if event.type == KEYUP:
            if event.key == K_RIGHT:
                moving_right = False
            if event.key == K_LEFT:
                moving_left = False

    screen.blit(pygame.transform.scale(display, WINDOW_SIZE), (0, 0))
    pygame.display.update()
    clock.tick(60)
